﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using LCPrinter;

public class ScriptsCamera : MonoBehaviour
{
    private bool camAvailable;
    private WebCamTexture backCam;
    private WebCamTexture frontCam;
    private Texture defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;

    public GameObject jsob;
    public GameObject loadob;
    public GameObject camera_ani;
    public GameObject count_ani;
    public GameObject setting;
    public GameObject printing;

    public RawImage pic;
    Texture2D t2D;
    public string printerName = "";

    // Start is called before the first frame update
    void Start()
    {
        Screen.SetResolution(3840, 2160, true);
        StartCoroutine(start_fn());

        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;
        if (devices.Length == 0)
        {
            Debug.Log("No camera detected");
            camAvailable = false;
            return;
        }
        for (int i = 0; i < devices.Length; i++)
        {

            if (!devices[i].isFrontFacing)
            {
                //開啟後鏡頭
            }   
            if (devices[i].isFrontFacing)
            { 
                //開啟前鏡頭
                backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }
        if (backCam == null)
        {
            Debug.Log("Unable to find back camera");
            return;
        }
        backCam.Play();
        background.texture = backCam;

        camAvailable = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!camAvailable)
            return;

        float ratio = (float)backCam.width / (float)backCam.height;
        fit.aspectRatio = ratio;

        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
        //background.rectTransform.localScale = new Vector3 (1f, scaleY, 1f);    //非鏡像
        background.rectTransform.localScale = new Vector3(-1f, scaleY, 1f);    //鏡像

        int orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    }
    private IEnumerator start_fn()
    {
        yield return new WaitForSeconds(2);
        loadob.SetActive(false);
        yield return new WaitForSeconds(3);
        jsob.SetActive(true);
        yield return new WaitForSeconds(13);
        camera_ani.SetActive(true);
        yield return new WaitForSeconds(2);
        camera_ani.SetActive(false);
        count_ani.SetActive(true);
        yield return new WaitForSeconds(6);
        count_ani.SetActive(false);
        StartCoroutine(scan());
        yield return new WaitForSeconds(3);
        jsob.SetActive(false);
        setting.SetActive(true);
        backCam.Stop();
    }
    public void retake_A()
    {
        SceneManager.LoadScene(1);
    }
    public void retake_B()
    {
        SceneManager.LoadScene(2);
    }
    public void printing_fn()
    {
        setting.SetActive(false);
        printing.SetActive(true);
        //byte[] byt = t2D.EncodeToPNG();
        //File.WriteAllBytes("E:/" + System.DateTime.Now.ToString("MMddHHmmss") + ".png", byt);
        //列印
        Print.PrintTexture(t2D.EncodeToPNG(), 1, printerName);
        StartCoroutine(backfront());
    }
    private IEnumerator backfront()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(0);
    }
    private IEnumerator scan()
    {
        yield return new WaitForSeconds(2);
        t2D = new Texture2D(Screen.width, Screen.height);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        yield return new WaitForEndOfFrame();//等待攝影機的影像繪製完畢

        t2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);//掃描的範圍，設定為整個攝影機拍到的影像，若影像嚴重延遲，請降低掃描大小。
        t2D.Apply();//開始掃描

        pic.texture = t2D;
    }
}
