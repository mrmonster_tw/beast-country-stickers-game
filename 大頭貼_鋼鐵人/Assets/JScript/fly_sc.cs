﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fly_sc : MonoBehaviour
{
    public float t1 = 2;
    public float t2 = 7;
    public float t3 = 3;
    public float t4 = 2;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(flyfly());
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    private IEnumerator flyfly()
    {

        yield return new WaitForSeconds(t1);
        this.GetComponent<Animator>().Play("fly_b");
        yield return new WaitForSeconds(t2);
        this.GetComponent<Animator>().Play("fly_A");
        yield return new WaitForSeconds(t3);
        this.GetComponent<Animator>().Play("idle");
        yield return new WaitForSeconds(t4);
        this.GetComponent<Animator>().Play("hello");
    }
}
