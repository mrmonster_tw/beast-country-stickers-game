﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Drawing.Printing;
using System.IO;

public class PrintManager {
    public PrintDocument priTarget;
    public Texture2D printTexture;
    public string printString;

    public PrintManager(int paperSizeW = 611, int paperSizeH = 408) {
        printTexture = null;
        priTarget = new PrintDocument();

        priTarget.PrintPage += new PrintPageEventHandler(Print);
        priTarget.BeginPrint += new PrintEventHandler(BeginPrint);
        priTarget.EndPrint += new PrintEventHandler(EndPrint);

        //priTarget.DefaultPageSettings.PaperSize = new PaperSize("10x15(4x6\")", 409, 618);
        priTarget.DefaultPageSettings.PaperSize = new PaperSize("10x15(4x6\")", paperSizeW, paperSizeH);

        //priTarget.DefaultPageSettings.Margins.Left = 10;
        //priTarget.DefaultPageSettings.Margins.Right = 10;
        //priTarget.DefaultPageSettings.Margins.Bottom = 10;
        //priTarget.DefaultPageSettings.Margins.Top = 10;

        //test Size
        //priTarget.DefaultPageSettings.PaperSize = new PaperSize("PR (3.5x5)", 363, 516);
    }


    private void BeginPrint(object sender, PrintEventArgs e) {
        Debug.Log("Start Print");
    }

    private void EndPrint(object sender, PrintEventArgs e) {
        Debug.Log("End Print");
    }

    public Texture2D UnityPixelToBitmap(Texture2D uImage) {
        if (uImage == null)
            return null;

        Texture2D image = new Texture2D(uImage.width, uImage.height, TextureFormat.RGBA32, false);

        Color[] pixels = uImage.GetPixels();

        for (int i = 0; i < pixels.Length; i++) {
            float oldR = pixels[i].r;
            float oldG = pixels[i].g;
            float oldB = pixels[i].b;
            pixels[i].r = oldB;
            pixels[i].g = oldG;
            pixels[i].b = oldR;
        }

        image.SetPixels(pixels);
        image.Apply();

        return image;
    }

    private void PrintText(object sender, PrintPageEventArgs e) {
        try {
            System.Drawing.Font drawFont = new System.Drawing.Font("Arial", 16);
            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            e.Graphics.DrawString(printString, drawFont, drawBrush, 0, 0);
        }
        catch (Exception exception) {
            Debug.LogError(exception.Message);
        }
    }

    private void Print(object sender, PrintPageEventArgs e) {
        try {
            Texture2D imageOverview = UnityPixelToBitmap(printTexture);

            byte[] textureData = new byte[4 * imageOverview.width * imageOverview.height];
            textureData = imageOverview.GetRawTextureData();

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(
                imageOverview.width, imageOverview.height,
                System.Drawing.Imaging.PixelFormat.Format32bppRgb
            );

            System.Drawing.Imaging.BitmapData bmpData = bmp.LockBits(
                new System.Drawing.Rectangle(0, 0, imageOverview.width, imageOverview.height),
                System.Drawing.Imaging.ImageLockMode.WriteOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppRgb
            );

            IntPtr safePtr = bmpData.Scan0;
            System.Runtime.InteropServices.Marshal.Copy(textureData, 0, safePtr, textureData.Length);
            bmp.UnlockBits(bmpData);

            //預設 
            e.Graphics.DrawImage(bmp, e.PageBounds);//依照紙張頁面大小強制填滿

            //邊緣留白
            //e.Graphics.DrawImage(bmp, e.MarginBounds); // 跟設定周圍邊緣有關
            
            /*priTarget.DefaultPageSettings.Margins.Left = 100;
            priTarget.DefaultPageSettings.Margins.Right = 100;
            priTarget.DefaultPageSettings.Margins.Bottom = 100;
            priTarget.DefaultPageSettings.Margins.Top = 100;*/
            

            //從設定的座標開始繪製(左上角)
            //e.Graphics.DrawImage(bmp, 0, 0);
            //Debug.Log(e.PageBounds.X + " " + e.PageBounds.Y + " " + e.PageBounds.Width + " " + e.PageBounds.Height);

        }
        catch (Exception ee) {
            UnityEngine.Debug.LogError(ee.Message);
        }
    }

    public Texture2D FlipTexture(Texture2D original) {
        Texture2D flipped = new Texture2D(original.width, original.height);

        int xN = original.width;
        int yN = original.height;

        for (int i = 0; i < xN; i++) {
            for (int j = 0; j < yN; j++) {
                flipped.SetPixel(xN - i - 1, j, original.GetPixel(i, j));
            }
        }
        flipped.Apply();

        return flipped;
    }

    public Texture2D ClipTexture(Texture2D originalImage, Rect ClipRect) {
        Texture2D clipTexture;
        clipTexture = new Texture2D(Mathf.FloorToInt(ClipRect.width), Mathf.FloorToInt(ClipRect.height), TextureFormat.RGBA32, false);

        Color[] getColor = originalImage.GetPixels(Mathf.FloorToInt(ClipRect.x)
            , Mathf.FloorToInt(ClipRect.y)
            , Mathf.FloorToInt(ClipRect.width)
            , Mathf.FloorToInt(ClipRect.height));

        clipTexture.SetPixels(getColor);
        clipTexture.Apply();

        return clipTexture;
    }

    public void PrintText(string text) {
        printString = text;
        priTarget.Print();
    }

    public void PrintFile(Texture2D printFile, bool isFlip = true, bool isClip = true, bool isWaterMark = true) {
        if (printFile == null) {
            Debug.Log("printFile is null.");
            return;
        }

        Texture2D imageTexture = printFile;

        ////切圖片成4x6吋大小
        //if (isClip)
        //{
        //    imageTexture = ClipTexture(imageTexture, new Rect(13, 168, 1054, 1584));
        //}

        ////加上浮水印
        //if (isWaterMark) {
        //    Texture2D waterMark = Resources.Load<Texture2D>("WaterMark/spe3d_QR");
        //    waterMark = ResizeTexture.ResizeTextureSize(waterMark, ResizeTexture.ImageFilterMode.Average, 0.25f);

        //    if (waterMark != null) {

        //        Color[] waterMarkColors = waterMark.GetPixels();
        //        //printFile.SetPixels(printFile.width  - waterMark.width,
        //        //                        printFile.height - waterMark.height,
        //        //                        waterMark.width, waterMark.height,waterMarkColors);

        //        //右下角
        //        //imageTexture.SetPixels(imageTexture.width - waterMark.width,
        //        //         0,
        //        //        waterMark.width, waterMark.height, waterMarkColors);

        //        //左下角
        //        imageTexture.SetPixels(40, 40,
        //                waterMark.width, waterMark.height, waterMarkColors);
        //    }

        //}

        ////翻轉圖片(因為列印會水平翻轉一次)
        //if (isFlip) {
        //    imageTexture = FlipTexture(imageTexture);
        //    printTexture = imageTexture;
        //}
        //else {
        //    printTexture = imageTexture;
        //}

        printTexture = imageTexture;

        try {
            priTarget.Print();
        }
        catch (Exception _e) {
            UnityEngine.Debug.Log(_e);
        }
    }

}