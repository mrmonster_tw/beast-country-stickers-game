﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using LCPrinter;

public class ScriptsCamera_1 : MonoBehaviour
{
    private bool camAvailable;
    private WebCamTexture backCam;
    private WebCamTexture frontCam;
    private Texture defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;
    public GameObject loadob;

    public GameObject demo_ani;
    public GameObject count_ani;
    public GameObject camera_icon;
    public GameObject setting;
    public GameObject printing;
    public GameObject slash;
    public GameObject list;
    public GameObject printer_setting;

    public GameObject ob_1;
    public GameObject ob_2;
    public GameObject ob_3;
    public GameObject ob_4;
    public GameObject ob_5;
    public GameObject ob_7;
    public GameObject ob_8;
    public GameObject ChubbyTartGrayling;

    public RawImage pic;
    Texture2D t2D;

    public float time = 6;
    //public string printerName = "";
    //public Text printer;

    //[SerializeField] private RawImage _rawImage;
    [SerializeField] private int paperSizeW = 611;
    [SerializeField] private int paperSizeH = 408;

    private PrintManager _printManager;
    public GameObject line;

    // Start is called before the first frame update
    void Start()
    {
        //printerName = PlayerPrefs.GetString("print");
        Screen.SetResolution(2160, 3840, true);
        StartCoroutine(loading_fn());

        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;
        if (devices.Length == 0)
        {
            Debug.Log("No camera detected");
            camAvailable = false;
            return;
        }
        for (int i = 0; i < devices.Length; i++)
        {

            if (!devices[i].isFrontFacing)
            {
                //開啟後鏡頭
                //backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }   
            if (devices[i].isFrontFacing)
            { 
                //開啟前鏡頭
                backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }
        if (backCam == null)
        {
            Debug.Log("Unable to find back camera");
            return;
        }
        backCam.Play();
        background.texture = backCam;

        camAvailable = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //printer_setting.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            backCam.Stop();
            SceneManager.LoadScene("movie");
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            backCam.Stop();
            SceneManager.LoadScene("A");
        }

        if (!camAvailable)
            return;

        float ratio = (float)backCam.width / (float)backCam.height;
        fit.aspectRatio = ratio;

        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3 (1f, scaleY, 1f);    //非鏡像
        //background.rectTransform.localScale = new Vector3(-1f, scaleY, 1f);    //鏡像

        int orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);

       
    }
    private IEnumerator loading_fn()
    {
        yield return new WaitForSeconds(2);
        loadob.SetActive(false);
    }
    private IEnumerator start_fn()
    {
        yield return new WaitForSeconds(1);
        camera_icon.SetActive(true);
        yield return new WaitForSeconds(5);
        camera_icon.SetActive(false);
        demo_ani.SetActive(true);
        yield return new WaitForSeconds(6);
        count_ani.SetActive(true);
        yield return new WaitForSeconds(6);
        count_ani.SetActive(false);
        StartCoroutine(scan());
        yield return new WaitForSeconds(time);
        demo_ani.SetActive(false);
        slash.SetActive(false);
        yield return new WaitForSeconds(1);
        setting.SetActive(true);
        //backCam.Stop();
    }
    public void closelist_fn(int xx)
    {
        list.SetActive(false);
        line.SetActive(true);
        StartCoroutine(start_fn());
        if (xx == 1)
        {
            demo_ani = ob_1;
        }
        if (xx == 2)
        {
            demo_ani = ob_2;
        }
        if (xx == 3)
        {
            demo_ani = ob_3;
        }
        if (xx == 4)
        {
            demo_ani = ob_4;
        }
        if (xx == 5)
        {
            demo_ani = ob_5;
        }
        if (xx == 6)
        {
            demo_ani = ChubbyTartGrayling;
        }
        if (xx == 7)
        {
            demo_ani = ob_7;
        }
        if (xx == 8)
        {
            demo_ani = ob_8;
        }
        
    }
    public void retake_A()
    {
        backCam.Stop();
        SceneManager.LoadScene("A");
    }
    public void printing_fn()
    {
        setting.SetActive(false);
        printing.SetActive(true);
        this.GetComponent<AudioSource>().enabled = false;

        byte[] byt = newImg.EncodeToPNG();
        File.WriteAllBytes("E:/" + System.DateTime.Now.ToString("MMddHHmmss") + ".png", byt);
        //File.WriteAllBytes("D:/" + "D" + ".png", byt);
        //列印
        //LCPrinter.Print.PrintTexture(t2D.EncodeToPNG(), 1, printerName);
        //LCPrinter.Print.PrintTextureByPath("E:\\pic.png", 1, printerName);

        //StartCoroutine(backfront());

        _printManager = new PrintManager(paperSizeW, paperSizeH);

        _printManager.PrintFile(newImg);

        backCam.Stop();

        //test.texture = newImg;
    }
    private IEnumerator backfront()
    {
        yield return new WaitForSeconds(10);
        backCam.Stop();
        SceneManager.LoadScene("movie");
    }
    private IEnumerator scan()
    {
        line.SetActive(false);
        yield return new WaitForSeconds(1);
        //t2D = new Texture2D(Screen.width, Screen.height);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        t2D = new Texture2D(1600, 1080);//掃描後的影像儲存大小，越大會造成效能消耗越大，若影像嚴重延遲，請降低儲存大小。
        yield return new WaitForEndOfFrame();//等待攝影機的影像繪製完畢

        t2D.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);//掃描的範圍，設定為整個攝影機拍到的影像，若影像嚴重延遲，請降低掃描大小。
        t2D.Apply();//開始掃描
        RotateTextures(t2D,false);

        pic.texture = t2D;

        slash.SetActive(true);

        //byte[] byt = t2D.EncodeToPNG();
        //File.WriteAllBytes("E:/" + "pic" + ".png", byt);
    }
    public void setting_fn()
    {
        //printerName = printer.text;
        //PlayerPrefs.SetString("print", printerName);
        //printer_setting.SetActive(false);
    }

    /*void RotateTexture(Texture2D texture, float eulerAngles)
    {
        int x;
        int y;
        int i;
        int j;
        float phi = eulerAngles / (180 / Mathf.PI);
        float sn = Mathf.Sin(phi);
        float cs = Mathf.Cos(phi);
        Color32[] arr = texture.GetPixels32();
        Color32[] arr2 = new Color32[arr.Length];
        int W = texture.width; int H = texture.height;
        int xc = W / 2;
        int yc = H / 2;
        for (j = 0; j < H; j++)
        {
            for (i = 0; i < W; i++)
            {
                arr2[j * W + i] = new Color32(0, 0, 0, 0);
                x = (int)(cs * (i - xc) + sn * (j - yc) + xc);
                y = (int)(-sn * (i - xc) + cs * (j - yc) + yc);
                if ((x > -1) && (x < W) && (y > -1) && (y < H))
                { arr2[j * W + i] = arr[y * W + x];
                }
            }
        }
        newImg = new Texture2D(W, H); newImg.SetPixels32(arr2);
        newImg.Apply();
        //return newImg;
        Debug.Log("aa");
    }*/
    Texture2D newImg;

    public void RotateTextures(Texture2D originalTexture, bool clockwise = false)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();

        newImg = rotatedTexture;
        //return rotatedTexture;
    }
}
