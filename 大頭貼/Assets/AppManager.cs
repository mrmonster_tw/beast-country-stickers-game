﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppManager : MonoBehaviour {

    [SerializeField] private RawImage _rawImage;
    [SerializeField] private int paperSizeW = 611;
    [SerializeField] private int paperSizeH = 408;

    private PrintManager _printManager;

    void Start() {
        
    }

    void Update() {

    }

    public void OnPrintClicked() {
        _printManager = new PrintManager(paperSizeW, paperSizeH);

        Texture2D tex = Resources.Load<Texture2D>("800x1200");
        _rawImage.texture = tex;

        tex = _printManager.FlipTexture(tex);

        Color32[] colors = tex.GetPixels32();
        _printManager.PrintFile(tex);
    }
}
